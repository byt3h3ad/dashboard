"use client";

import { MainNav } from "@/components/main-nav";
import { TaskPage } from "@/components/tasks";

export default function Home() {
  return (
    <main className="grid">
      <MainNav />
      <TaskPage />
    </main>
  );
}
