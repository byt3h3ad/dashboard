import Link from "next/link";
import {
  NavigationMenu,
  NavigationMenuItem,
  NavigationMenuLink,
  NavigationMenuList,
  navigationMenuTriggerStyle,
} from "@/components/ui/navigation-menu";
import TeamSwitcher from "@/components/team-switcher";
import { Search } from "@/components/search";
import { UserNav } from "@/components/user-nav";

export function MainNav() {
  return (
    <NavigationMenu className="border-b px-4 py-2 flex items-center justify-around space-x-8">
      <NavigationMenuList>
        <NavigationMenuItem>
          <TeamSwitcher />
        </NavigationMenuItem>
        <NavigationMenuItem>
          <Link href="/" legacyBehavior passHref>
            <NavigationMenuLink className={navigationMenuTriggerStyle()}>
              Home
            </NavigationMenuLink>
          </Link>
        </NavigationMenuItem>
        <NavigationMenuItem>
          <Link href="/dashboard" legacyBehavior passHref>
            <NavigationMenuLink className={navigationMenuTriggerStyle()}>
              Dashboard
            </NavigationMenuLink>
          </Link>
        </NavigationMenuItem>
        <NavigationMenuItem>
          <Link href="/docs" legacyBehavior passHref>
            <NavigationMenuLink className={navigationMenuTriggerStyle()}>
              Documentation
            </NavigationMenuLink>
          </Link>
        </NavigationMenuItem>
        <NavigationMenuItem className="px-4">
          <Search />
        </NavigationMenuItem>
        <NavigationMenuItem className="flex">
          <UserNav />
        </NavigationMenuItem>
      </NavigationMenuList>
    </NavigationMenu>
  );
}
