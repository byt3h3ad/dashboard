import { z } from "zod"

// We're keeping a simple non-relational schema here.
// IRL, you will have a schema for your data models.
export const taskSchema = z.object({
  date: z.date(),
  name: z.string(),
  status: z.string(),
  // label: z.string(),
  blood_group: z.string(),
  quality: z.string(),
  quantity: z.number(),
})

export type Task = z.infer<typeof taskSchema>