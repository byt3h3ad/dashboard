import fs from "fs"
import path from "path"
import { faker } from "@faker-js/faker"

import { bloodGroup, quality, statuses } from "./data"

const tasks = Array.from({ length: 100 }, () => ({
  date: faker.date.recent({ days: 30 }).toISOString(),
  name: faker.person.fullName(),
  status: faker.helpers.arrayElement(statuses).value,
  blood_group: faker.helpers.arrayElement(bloodGroup).value,
  quality: faker.helpers.arrayElement(quality).value,
  quantity: `${faker.number.int({ min: 100, max: 1000 })} ml`,
  // label: faker.helpers.arrayElement(labels).value,
  // priority: faker.helpers.arrayElement(priorities).value,
}))

fs.writeFileSync(
  path.join(__dirname, "tasks.json"),
  JSON.stringify(tasks, null, 2)
)

console.log("✅ Tasks data generated.")